package com.mythden.mythdenbot.pramukainfo.service;

import com.mythden.mythdenbot.pramukainfo.model.Info;
import com.mythden.mythdenbot.pramukainfo.repository.InfoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InfoServiceImplTest {

    Info info = new Info();

    @Mock
    InfoRepository infoRepository;

    @InjectMocks
    InfoServiceImpl infoServiceImpl;

    @BeforeEach
    private void setUp() {
        info.setIdInfo(0);
        info.setTitle("Dummy McDumbDumb");
        info.setContent("Lalala");
    }

    @Test
    void createInfo() {
        lenient().when(infoRepository.save(any(Info.class))).thenReturn(info);
        assertEquals(0, (int) infoServiceImpl.createInfo(info).getIdInfo());
    }

    @Test
    void updateInfo() {
        Info infoNew = new Info();
        infoNew.setIdInfo(1);
        infoNew.setContent("New");
        lenient().when(infoRepository.save(any(Info.class))).thenReturn(info);
        assertEquals(0, (int) infoServiceImpl.createInfo(info).getIdInfo());
        lenient().when(infoRepository.save(any(Info.class))).thenReturn(infoNew);
        assertTrue(infoServiceImpl.updateInfo(0, infoNew).getContent().contains("New"));
    }

    @Test
    void deleteInfo() {
        lenient().when(infoRepository.save(any(Info.class))).thenReturn(info);
        assertEquals(0, (int) infoServiceImpl.createInfo(info).getIdInfo());
        lenient().when(infoRepository.findByIdInfo(0)).thenReturn(info);
        infoServiceImpl.deleteInfo(0);
        verify(infoRepository, times(1)).delete(info);
    }

    @Test
    void getInfoByTitle() {
        lenient().when(infoRepository.findByTitleContainingIgnoreCase("Dummy")).thenReturn(info);
        assertTrue(infoServiceImpl.getInfoByTitle("Dummy").getTitle().contains("Dummy"));
    }

    @Test
    void getInfoById() {
        lenient().when(infoRepository.findByIdInfo(0)).thenReturn(info);
        assertEquals(0, (int) infoServiceImpl.getInfoById(0).getIdInfo());
    }

    @Test
    void getAllInfo() {
        List<Info> list = new ArrayList<>();
        list.add(info);
        lenient().when(infoRepository.findAllByOrderByIdInfoAsc()).thenReturn(list);
        assertIterableEquals(list, infoServiceImpl.getAllInfo());
    }
}