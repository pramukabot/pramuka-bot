package com.mythden.mythdenbot.pramukainfo.service;

import com.mythden.mythdenbot.pramukainfo.model.Info;
import com.mythden.mythdenbot.pramukainfo.model.Notebook;
import com.mythden.mythdenbot.pramukainfo.repository.NotebookRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NotebookServiceImplTest {

    private final String UID = "KENTA";
    Notebook nb = new Notebook();
    Info info = new Info();
    List<Info> infos = new ArrayList<>();

    @Mock
    NotebookRepository notebookRepository;

    @InjectMocks
    NotebookServiceImpl notebookServiceImpl;

    @BeforeEach
    private void setUp() {
        nb.setOwner(UID);
        nb.setInfos(infos);
    }

    private void setUpInfo() {
        info.setIdInfo(0);
        info.setTitle("Dummy McDumbDumb");
        info.setContent("Lalala");
        infos.add(info);
    }

    @AfterEach
    private void tearDown() {
        infos.clear();
    }

    @Test
    void saveNotebook() {
        lenient().when(notebookRepository.save(nb)).thenReturn(nb);
        assertEquals(UID, notebookServiceImpl.saveNotebook(nb).getOwner());
    }

    @Test
    void addInfoNew() {
        Notebook newNb = new Notebook();
        newNb.setOwner(UID);
        setUpInfo();
        newNb.setInfos(infos);
        List<Info> mockInfos = new ArrayList<>();
        nb.setInfos(mockInfos);
        lenient().when(notebookRepository.findByOwner(UID)).thenReturn(nb);
        lenient().when(notebookRepository.save(any(Notebook.class))).thenReturn(newNb);
        assertTrue(notebookServiceImpl.addInfo(info, UID).getInfos().contains(info));
    }

    @Test
    void addInfoAlreadyExists() {
        Notebook newNb = new Notebook();
        newNb.setOwner(UID);
        setUpInfo();
        newNb.setInfos(infos);
        lenient().when(notebookRepository.findByOwner(UID)).thenReturn(newNb);
        assertThrows(IllegalArgumentException.class, () ->  notebookServiceImpl.addInfo(info, UID));
    }

    @Test
    void removeInfo() {
        Notebook newNb = new Notebook();
        newNb.setOwner(UID);
        setUpInfo();
        newNb.setInfos(infos);
        lenient().when(notebookRepository.findByOwner(UID)).thenReturn(newNb);
        lenient().when(notebookRepository.save(any(Notebook.class))).thenReturn(nb);
        assertFalse(notebookServiceImpl.removeInfo(0, UID).getInfos().contains(info));
    }

    @Test
    void getNotebookByOwner() {
        lenient().when(notebookRepository.findByOwner(UID)).thenReturn(nb);
        assertEquals(UID, notebookServiceImpl.getNotebookByOwner(UID).getOwner());
    }

    @Test
    void deleteNotebookByOwner() {
        lenient().when(notebookRepository.save(nb)).thenReturn(nb);
        assertEquals(UID, notebookServiceImpl.saveNotebook(nb).getOwner());
        lenient().when(notebookRepository.findByOwner(UID)).thenReturn(nb);
        notebookServiceImpl.deleteNotebookByOwner(UID);
        verify(notebookRepository, times(1)).delete(nb);
    }
}