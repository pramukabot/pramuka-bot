package com.mythden.mythdenbot.pramukainfo.core.reply;

import com.mythden.mythdenbot.pramukainfo.model.Info;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ReplyInfoTest {

    Info plainInfo = new Info();
    Info imageInfo = new Info();
    ReplyInfo plainReply;
    ReplyInfo imageReply;

    @BeforeEach
    private void setUp() {
        plainInfo.setIdInfo(0);
        plainInfo.setTitle("Plain");
        plainInfo.setContent("Lalala");

        plainReply = new ReplyInfo(plainInfo);

        imageInfo.setIdInfo(1);
        imageInfo.setTitle("Image");
        imageInfo.setContent("img(https://imgur.com/nSL1q1R.gif.gif)");

        imageReply = new ReplyInfo(imageInfo);
    }

    @Test
    void getTitle() {
        assertEquals(plainReply.getTitle(), "Plain");
    }

    @Test
    void getContent() {
        assertEquals(plainReply.getContent(), "Lalala");
        assertNull(plainReply.get().getEmbeds().get(0).getImage());
        assertNotNull(imageReply.get().getEmbeds().get(0).getImage());
    }

    @Test
    void addFooter() {
        assertTrue(plainReply.get().getEmbeds().get(0).getFooter().getText().contains("0"));
    }
}