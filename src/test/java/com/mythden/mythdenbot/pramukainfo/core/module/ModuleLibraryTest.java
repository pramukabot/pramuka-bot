package com.mythden.mythdenbot.pramukainfo.core.module;

import com.mythden.mythdenbot.pramukainfo.core.ParserHelper;
import com.mythden.mythdenbot.pramukainfo.core.reply.Reply;
import com.mythden.mythdenbot.pramukainfo.core.reply.ReplyHelper;
import com.mythden.mythdenbot.pramukainfo.model.Info;
import com.mythden.mythdenbot.pramukainfo.model.Notebook;
import com.mythden.mythdenbot.pramukainfo.service.InfoService;
import com.mythden.mythdenbot.pramukainfo.service.NotebookService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
public class ModuleLibraryTest {

    @Mock
    InfoService infoService;
    @Mock
    NotebookService notebookService;
    @InjectMocks
    ModuleLibrary moduleLibrary;

    Info info = new Info();

    MockedStatic<ParserHelper> mock = Mockito.mockStatic(ParserHelper.class);

    @BeforeEach
    public void setUp() {
        info.setIdInfo(0);
        info.setTitle("Haha");
        info.setContent("Hehe");
        moduleLibrary.setServices(infoService, notebookService);
        mock.when(() -> ParserHelper.getUsername(anyString())).thenReturn("KENTA");
    }

    @AfterEach
    public void tearDown() {
        mock.close();
    }

    @Test
    public void testCreateInfo(){
        lenient().when(infoService.createInfo(any(Info.class))).thenReturn(info);
        Module<List<Reply>> module  = ModuleLibrary.CREATE_INFO.create();
        module.addArgs(new Object[]{"Haha\nHehe"});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.CREATE);
        assertSameDesc(debug, ReplyHelper.CREATE.success());
    }

    @Test
    public void testUpdateInfo(){
        lenient().when(infoService.updateInfo(anyInt(), any(Info.class))).thenReturn(info);
        Module<List<Reply>> module  = ModuleLibrary.UPDATE_INFO.create();
        module.addArgs(new Object[]{"1","Haha\nHehe"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.UPDATE);
        assertSameDesc(debug, ReplyHelper.UPDATE.success());
    }

    @Test
    public void testDeleteInfoFound(){
        Module<List<Reply>> module  = ModuleLibrary.DELETE_INFO.create();
        module.addArgs(new Object[]{"1"});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.DELETE);
        assertSameDesc(debug, ReplyHelper.DELETE.success());
    }

    @Test
    public void testDeleteInfoNotFound(){
        lenient().when(infoService.getInfoById(anyInt())).thenReturn(null);
        doThrow(new IllegalArgumentException()).when(infoService).deleteInfo(anyInt());
        Module<List<Reply>> module  = ModuleLibrary.DELETE_INFO.create();
        module.addArgs(new Object[]{"999"});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.DELETE);
        assertSameDesc(debug, ReplyHelper.DELETE.notFound());
    }

    @Test
    public void testGetInfoByIdFound(){
        lenient().when(infoService.getInfoById(anyInt())).thenReturn(info);
        Module<List<Reply>> module  = ModuleLibrary.GET_INFO_BY_ID.create();
        module.addArgs(new Object[]{"1"});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.success());
    }

    @Test
    public void testGetInfoByIdNotFound(){
        lenient().when(infoService.getInfoById(anyInt())).thenReturn(null);
        Module<List<Reply>> module  = ModuleLibrary.GET_INFO_BY_ID.create();
        module.addArgs(new Object[]{"999"});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.notFound());
    }

    @Test
    public void testGetInfoByTitleFound(){
        lenient().when(infoService.getInfoByTitle(anyString())).thenReturn(info);
        Module<List<Reply>> module  = ModuleLibrary.GET_INFO_BY_TITLE.create();
        module.addArgs(new Object[]{"Haha"});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.success());
    }

    @Test
    public void testGetInfoByTitleNotFound(){
        lenient().when(infoService.getInfoByTitle(anyString())).thenReturn(null);
        Module<List<Reply>> module  = ModuleLibrary.GET_INFO_BY_TITLE.create();
        module.addArgs(new Object[]{"Hadeh"});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.notFound());
    }

    @Test
    public void testGetAllInfo() {
        Iterable<Info> infos = List.of(info);
        lenient().when(infoService.getAllInfo()).thenReturn(infos);
        Module<List<Reply>> module  = ModuleLibrary.GET_ALL_INFO.create();
        assertEquals(0, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.success());
    }

    Notebook nb = new Notebook();
    String UID = "123456789987654321";
    List<Info> infos = new ArrayList<>();

    @BeforeEach
    public void setUpNotebook() {
        nb.setOwner(UID);
        nb.setInfos(infos);
    }

    @Test
    public void testCreateNotebookNew() {
        lenient().when(notebookService.getNotebookByOwner(UID)).thenReturn(null);
        lenient().when(notebookService.saveNotebook(nb)).thenReturn(nb);
        Module<List<Reply>> module  = ModuleLibrary.CREATE_NOTEBOOK.create();
        module.addArgs(new Object[]{UID});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.CREATE);
        assertSameDesc(debug, ReplyHelper.CREATE.success());
    }

    @Test
    public void testCreateNotebookOverride() {
        lenient().when(notebookService.getNotebookByOwner(UID)).thenReturn(nb);
        Module<List<Reply>> module  = ModuleLibrary.CREATE_NOTEBOOK.create();
        module.addArgs(new Object[]{UID});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.CREATE);
        assertSameDesc(debug, ReplyHelper.CREATE.with("Thou shall not overwrite thy old scriptures!"));
    }

    @Test
    public void testDeletNotebookFound() {
        lenient().when(notebookService.getNotebookByOwner(UID)).thenReturn(nb);
        Module<List<Reply>> module  = ModuleLibrary.DELETE_NOTEBOOK.create();
        module.addArgs(new Object[]{UID});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.DELETE);
        assertSameDesc(debug, ReplyHelper.DELETE.success());
    }

    @Test
    public void testDeletNotebookNotFound() {
        lenient().when(notebookService.getNotebookByOwner(UID)).thenReturn(null);
        doThrow(new IllegalArgumentException()).when(notebookService).deleteNotebookByOwner(UID);
        Module<List<Reply>> module  = ModuleLibrary.DELETE_NOTEBOOK.create();
        module.addArgs(new Object[]{UID});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.DELETE);
        assertSameDesc(debug, ReplyHelper.DELETE.notFound());
    }

    @Test
    public void testAddInfoToNotebookSuccess() {
        lenient().when(infoService.getInfoByTitle(anyString())).thenReturn(info);
        lenient().when(notebookService.addInfo(any(Info.class), anyString())).thenReturn(nb);
        Module<List<Reply>> module  = ModuleLibrary.ADD_INSTANT_TO_NOTEBOOK.create();
        module.addArgs(new Object[]{UID, "Hehe"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.UPDATE);
        assertSameDesc(debug, ReplyHelper.UPDATE.success());
    }

    @Test
    public void testAddInfoToNotebookIllegal() {
        lenient().when(infoService.getInfoByTitle(anyString()))
                .thenThrow(new IllegalArgumentException());
        Module<List<Reply>> module  = ModuleLibrary.ADD_INSTANT_TO_NOTEBOOK.create();
        module.addArgs(new Object[]{UID, "Hehe"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.UPDATE);
        assertSameDesc(debug, ReplyHelper.UPDATE.with("What is this contraband?"));
    }

    @Test
    public void testAddInfoToNotebookError() {
        lenient().when(infoService.getInfoByTitle(anyString())).thenReturn(info);
        lenient().when(notebookService.getNotebookByOwner(anyString()))
                .thenThrow(new EntityNotFoundException());
        Module<List<Reply>> module  = ModuleLibrary.ADD_INSTANT_TO_NOTEBOOK.create();
        module.addArgs(new Object[]{UID, "Hehe"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.UPDATE);
        assertSameDesc(debug, ReplyHelper.UPDATE.with("Thy call hadth sundered the fabric of reality..."));
    }

    @Test
    public void testGetTOCFound() {
        lenient().when(notebookService.getNotebookByOwner(anyString())).thenReturn(nb);
        Module<List<Reply>> module  = ModuleLibrary.GET_TOC.create();
        module.addArgs(new Object[]{UID});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.success());
    }

    @Test
    public void testGetTOCNotFound() {
        lenient().when(notebookService.getNotebookByOwner(anyString()))
                .thenThrow(new EntityNotFoundException());
        Module<List<Reply>> module  = ModuleLibrary.GET_TOC.create();
        module.addArgs(new Object[]{UID});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.notFound());
    }

    @Test
    public void testDeletePageFound() {
        lenient().when(notebookService.removeInfo(anyInt(), anyString())).thenReturn(nb);
        Module<List<Reply>> module  = ModuleLibrary.DELETE_PAGE.create();
        module.addArgs(new Object[]{UID, "1"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.UPDATE);
        assertSameDesc(debug, ReplyHelper.UPDATE.success());
    }

    @Test
    public void testDeletePageNotFound() {
        lenient().when(notebookService.removeInfo(anyInt(), anyString()))
                .thenThrow(new EntityNotFoundException());
        Module<List<Reply>> module  = ModuleLibrary.DELETE_PAGE.create();
        module.addArgs(new Object[]{UID, "1"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.UPDATE);
        assertSameDesc(debug, ReplyHelper.UPDATE.notFound());
    }

    @Test
    public void testDeletePageOutOfBounds() {
        lenient().when(notebookService.removeInfo(anyInt(), anyString()))
                .thenThrow(new IndexOutOfBoundsException());
        Module<List<Reply>> module  = ModuleLibrary.DELETE_PAGE.create();
        module.addArgs(new Object[]{UID, "1000000"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.UPDATE);
        assertSameDesc(debug, ReplyHelper.UPDATE.indexOutOfBounds());
    }

    @Test
    public void testGetPageFound() {
        Notebook nb2 = new Notebook();
        nb2.setOwner(UID);
        infos.add(info);
        nb2.setInfos(infos);
        lenient().when(notebookService.getNotebookByOwner(anyString())).thenReturn(nb2);
        Module<List<Reply>> module  = ModuleLibrary.GET_PAGE.create();
        module.addArgs(new Object[]{UID, "1"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.success());
    }

    @Test
    public void testGetPageNotFound() {
        lenient().when(notebookService.getNotebookByOwner(anyString()))
                .thenThrow(new EntityNotFoundException());
        Module<List<Reply>> module  = ModuleLibrary.GET_PAGE.create();
        module.addArgs(new Object[]{UID, "1"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.notFound());
    }

    @Test
    public void testGetPageOutOfBounds() {
        lenient().when(notebookService.getNotebookByOwner(anyString()))
                .thenThrow(new IndexOutOfBoundsException());
        Module<List<Reply>> module  = ModuleLibrary.GET_PAGE.create();
        module.addArgs(new Object[]{UID, "1000000"});
        assertEquals(2, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.indexOutOfBounds());
    }

    @Test
    public void testOpenNotebookFound() {
        lenient().when(notebookService.getNotebookByOwner(anyString())).thenReturn(nb);
        Module<List<Reply>> module  = ModuleLibrary.OPEN_NOTEBOOK.create();
        module.addArgs(new Object[]{UID});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.success());
    }

    @Test
    public void testOpenNotebookNotFound() {
        lenient().when(notebookService.getNotebookByOwner(anyString()))
                .thenThrow(new EntityNotFoundException());
        Module<List<Reply>> module  = ModuleLibrary.OPEN_NOTEBOOK.create();
        module.addArgs(new Object[]{UID});
        assertEquals(1, module.params());
        List<Reply> replies = module.invoke();
        Reply debug = replies.get(0);
        assertEquals(debug, ReplyHelper.GET);
        assertSameDesc(debug, ReplyHelper.GET.notFound());
    }

    public static String getDesc(Reply reply) {
        return reply.get().getEmbeds().get(0).getDescription();
    }

    public static void assertSameDesc(Reply reply1, Reply reply2) {
        assertEquals(getDesc(reply1), getDesc(reply2));
    }

}