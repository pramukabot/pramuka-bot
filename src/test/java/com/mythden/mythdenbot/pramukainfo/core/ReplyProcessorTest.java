package com.mythden.mythdenbot.pramukainfo.core;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Page;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.pramukainfo.core.reply.*;
import com.mythden.mythdenbot.pramukainfo.model.Info;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ReplyProcessorTest {

    @Mock
    CommandEvent event;
    @Mock
    MessageChannel channel;
    @Mock
    MessageAction action;
    @Mock
    Message message;
    @Mock
    RestAction<Void> restAction;

    @Captor
    ArgumentCaptor<Consumer<Message>> captor;

    Info info;
    ReplyList list;
    ReplyList list2;
    List<Reply> replies;

    @BeforeEach
    void setUp() {
        info = new Info();
        info.setIdInfo(0);
        info.setTitle("Test Info");
        info.setContent("Test Content");
        ArrayList<Info> ilistables  = new ArrayList<>();
        for (int i = 0; i < 30; ++i) {
            ilistables.add(info);
        }
        list = new ReplyList(ListingHelper.ListType.INDEX, "Test List", ilistables);
        replies = List.of(
                ReplyHelper.DEBUG.with("Test"),
                list,
                new ReplyInfo(info)
        );
    }

    @Test
    void testProduceReply() {
        MockedStatic<Pages> mock = mockStatic(Pages.class);
        when(event.getChannel()).thenReturn(channel);
        when(channel.sendMessage(any(Message.class))).thenReturn(action);
        when(action.complete()).thenReturn(message);
        when(message.addReaction(anyString())).thenReturn(restAction);

        ReplyProcessor.produceReply(event, replies);
        verify(action).queue(captor.capture());
        ArrayList<Page> pages = list.getPages();
        pages.forEach(page -> captor.getValue().accept((Message) page.getContent()));
        mock.verify(times(3), () -> Pages.paginate(any(Message.class), any(List.class)));
        mock.close();
    }

}