package com.mythden.mythdenbot.pramukainfo.core.reaction;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.lenient;

@ExtendWith(MockitoExtension.class)
class ReactionHandlerTest {

    @InjectMocks
    ReactionHandler handler;

    @Mock
    GenericMessageReactionEvent event;
    @Mock
    MessageChannel channel;
    @Mock
    MessageAction action;
    @Mock
    MessageAction sendAction;
    @Mock
    Message message;
    @Mock
    Message sentMessage;
    @Mock
    SelfUser user;
    @Mock
    JDA jda;
    @Mock
    MessageReaction.ReactionEmote emote;
    @Mock
    MessageEmbed embed;
    @Mock
    MessageEmbed.Footer footer;

    final String FOOTER_TEXT = "ID: 0";
    final String FOOTER_TITLE = "Test";
    final String UID = "123456789987654321";

    @Test
    void testOnGenericMessageReaction() {
        lenient().when(event.getChannel()).thenReturn(channel);
        lenient().when(channel.retrieveMessageById(anyLong())).thenReturn(action);
        lenient().when(action.complete()).thenReturn(message);
        lenient().when(message.getAuthor()).thenReturn(user);
        lenient().when(event.getJDA()).thenReturn(jda);
        // isMessageFromSelf?
        lenient().when(jda.getSelfUser()).thenReturn(user);
        lenient().when(event.getReactionEmote()).thenReturn(emote);
        // isReactionCorrect?
        lenient().when(emote.getName()).thenReturn("📝");
        List<MessageEmbed> embeds = List.of(embed);
        lenient().when(message.getEmbeds()).thenReturn(embeds);
        lenient().when(embed.getFooter()).thenReturn(footer);
        // isInfo?
        lenient().when(footer.getText()).thenReturn(FOOTER_TEXT);
        lenient().when(embed.getTitle()).thenReturn(FOOTER_TITLE);
        // isReactionNotFromBot?
        lenient().when(event.getUserId()).thenReturn(UID);
        lenient().when(user.getId()).thenReturn("BOT");

        lenient().when(channel.sendMessage(any(Message.class))).thenReturn(sendAction);
        lenient().when(sendAction.complete()).thenReturn(sentMessage);

        handler.onGenericMessageReaction(event);
    }
}