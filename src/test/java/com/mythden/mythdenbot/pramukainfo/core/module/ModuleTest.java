package com.mythden.mythdenbot.pramukainfo.core.module;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class ModuleTest {

    Module<String> dummy = new Module<>() {
        @Override
        protected String produce() {
            return ((Integer) (1/0)).toString();
        }

        @Override
        public int params() {
            return 0;
        }
    };

    @Test
    void addArgs() {
        assertThrows(AssertionError.class, () -> dummy.addArgs(new Object[]{}));
    }

    @Test
    void invoke() {
        assertThrows(ArithmeticException.class, () -> dummy.produce());
        assertNull(dummy.invoke());
    }
}