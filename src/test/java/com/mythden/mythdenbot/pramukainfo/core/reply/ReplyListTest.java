package com.mythden.mythdenbot.pramukainfo.core.reply;

import com.mythden.mythdenbot.pramukainfo.model.Info;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class ReplyListTest {

    Info info;
    ReplyList replyList;

    @BeforeEach
    public void setUp() {
        info = new Info();
        info.setTitle("Dummy Info");
        info.setIdInfo(0);
        replyList = new ReplyList(
                ListingHelper.ListType.INDEX,
                "Dummy Title",
                Lists.newArrayList(info)
        );
        replyList = new ReplyList(
                ListingHelper.ListType.ID,
                "Dummy Title",
                Lists.newArrayList(info)
        );
    }

    @Test
    void addContent() {
        replyList.addContent("lalala");
        assertTrue(replyList.getContent().contains("lalala"));
    }

    @Test
    void getTitle() {
        assertTrue(replyList.getTitle().contains("Dummy Title"));
    }

    @Test
    void getContent() {
        assertNotNull(replyList.getContent());
    }

    @Test
    void setTitle() {
        replyList.setTitle("Haha");
        assertTrue(replyList.getTitle().contains("Haha"));
    }
}