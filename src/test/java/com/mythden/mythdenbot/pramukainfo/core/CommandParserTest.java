package com.mythden.mythdenbot.pramukainfo.core;

import com.mythden.mythdenbot.pramukainfo.core.module.ModuleLibrary;
import com.mythden.mythdenbot.pramukainfo.core.reply.ReplyHelper;
import com.mythden.mythdenbot.pramukainfo.service.InfoService;
import com.mythden.mythdenbot.pramukainfo.service.NotebookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.mythden.mythdenbot.pramukainfo.core.module.ModuleLibraryTest.assertSameDesc;

@ExtendWith(MockitoExtension.class)
class CommandParserTest {

    @Mock
    InfoService infoService;
    @Mock
    NotebookService notebookService;
    @InjectMocks
    ModuleLibrary moduleLibrary;

    CommandMap mirror = CommandMap.INFO_COMMANDS;

    @BeforeEach
    void setUp() {
        moduleLibrary.setServices(infoService, notebookService);
    }

    @Test
    void testGetReplyNoModule() {
        assertSameDesc(CommandParser.getReply(new String[]{"command"}, mirror).get(0),
                ReplyHelper.DEBUG.with("We cannot comprehend your request!"));
    }

    @Test
    void testGetReplyNoArg() {
        assertSameDesc(CommandParser.getReply(new String[]{"a"}, mirror).get(0),
                ReplyHelper.GET.success());
    }

    @Test
    void testGetReplyException() {
        assertSameDesc(CommandParser.getReply(new String[]{"c"}, mirror).get(0),
                ReplyHelper.getDebug("The slimes jammed the module-inator... again...")
                        .get(0));
    }

    @Test
    void testGetReplyArgs() {
        assertSameDesc(CommandParser.getReply(new String[]{"c","Test\nFun"}, mirror).get(0),
                ReplyHelper.CREATE.success());
    }
}