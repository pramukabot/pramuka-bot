package com.mythden.mythdenbot.pramukainfo.core;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CommandMapTest {

    @Test
    void get() {
        assertNotNull(CommandMap.INFO_COMMANDS.get("a"));
        assertNotNull(CommandMap.NOTEBOOK_COMMANDS.get("o"));
    }
}