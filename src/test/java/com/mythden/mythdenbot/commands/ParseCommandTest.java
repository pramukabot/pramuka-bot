package com.mythden.mythdenbot.commands;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Page;
import com.github.ygimenez.type.PageType;
import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ParseCommandTest {

    @InjectMocks
    ParseCommand command;

    @Mock
    CommandEvent event;
    @Mock
    MessageChannel channel;
    @Mock
    MessageAction action;

    @Captor
    ArgumentCaptor<Consumer<Message>> captor;

    String args1 = "alpha morse kenta";
    String args2 = "alpha morse kenta ganteng sekali";

    @Test
    void testExecute() {
        MockedStatic<Pages> mock = mockStatic(Pages.class);

        when(event.getArgs()).thenReturn(args1);
        command.execute(event);

        when(event.getArgs()).thenReturn(args2);
        when(event.getChannel()).thenReturn(channel);
        when(channel.sendMessage(any(Message.class))).thenReturn(action);
        command.execute(event);

        verify(action).queue(captor.capture());
        ArrayList<Page> pages = new ArrayList<>();
        MessageBuilder mb = new MessageBuilder();
        int size = 3;
        for (int i = 0; i < size; i++) {
            mb.append(Integer.toString(i));
            pages.add(new Page(PageType.TEXT, mb.build()));
        }
        pages.forEach(page -> captor.getValue().accept((Message) page.getContent()));
        mock.verify(times(size), () -> Pages.paginate(any(Message.class), any(List.class)));
        mock.close();
    }
}
