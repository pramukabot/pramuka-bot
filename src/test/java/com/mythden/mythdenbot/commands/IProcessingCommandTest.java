package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.pramukainfo.core.CommandMap;
import com.mythden.mythdenbot.pramukainfo.core.CommandParser;
import com.mythden.mythdenbot.pramukainfo.core.ReplyProcessor;
import com.mythden.mythdenbot.pramukainfo.core.reply.Reply;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@ExtendWith(MockitoExtension.class)
class IProcessingCommandTest {

    IProcessingCommand command = new IProcessingCommand() {
        @Override
        public CommandMap getMap() {
            return CommandMap.INFO_COMMANDS;
        }
    };

    @Mock
    List<Reply> replies;
    @Mock
    CommandEvent event;

    String[] args = new String[]{"a"};

    @Test
    void testProcess() {
        MockedStatic<CommandParser> mockCP = Mockito.mockStatic(CommandParser.class);
        MockedStatic<ReplyProcessor> mockRP = Mockito.mockStatic(ReplyProcessor.class);

        mockCP.when(() -> CommandParser.getReply(any(String[].class), any(CommandMap.class))).thenReturn(replies);
        command.process(event, args);
        mockCP.verify(times(1),
                () -> CommandParser.getReply(any(String[].class), any(CommandMap.class)));
        mockRP.verify(times(1),
                () -> ReplyProcessor.produceReply(any(CommandEvent.class), any(List.class)));

        mockCP.close();
        mockRP.close();
    }
}