package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.HashMap;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MiniGamesCommandTest {

//    @Spy @InjectMocks
//    MiniGamesCommand command;
//    @Mock
//    CommandEvent event;
//    @Mock
//    User user;

    @InjectMocks
    MiniGamesCommand command;
    @Mock
    CommandEvent event;
    @Mock
    User user;

    HashMap<String, Integer> playerScoreMap = new HashMap<>();

    String player = "KENTA";

    ArrayList<String> playerList = new ArrayList<>();

    /**
     * NB:
     *   Mungkin dapet coverage yang lebih gede dengan make kodingan ini.
     *   Cuman terjadi interaksi sama server lainnya, jadi database asli berubah karena test.
     *   Ide refactor: jsonHelper() kan void, di-ekstrak method yang return HttpClientnya biar bisa di-mock;
     *
     *   Buat yang bersih tanpa interaksi server microservice, tukeran ama yang dikomen.
     */
    @Test
    void testSimulation() {
        when(event.getAuthor()).thenReturn(user);
        when(user.getName()).thenReturn(player);
        command.execute(event);

        PertanyaanCommand pertanyaanCommand = new PertanyaanCommand();
        pertanyaanCommand.execute(event);

        when(event.getArgs()).thenReturn("Ini jawaban ngaco");

        JawabCommand jawabCommand = new JawabCommand();
        jawabCommand.execute(event);

        EndCommand endCommand = new EndCommand();
        endCommand.execute(event);

//        lenient().when(event.getAuthor()).thenReturn(user);
//        lenient().when(user.getName()).thenReturn(player);
//
//        lenient().doNothing().when(command).jsonHelper(anyString());
//        playerList.add(player);
//        lenient().when(command.getPemainHelper()).thenReturn(playerList);
//        MockedStatic<JsonParser> mockParser = Mockito.mockStatic(JsonParser.class);
//
//        playerScoreMap.put(player, 0);
//        mockParser.when(() -> JsonParser.readJsonFromUrlToArray(anyString())).thenReturn(playerScoreMap);
//
//        command.execute(event);

    }
}