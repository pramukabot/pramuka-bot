package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class NotebookCommandTest {

    @InjectMocks
    NotebookCommand command;

    @Mock
    CommandEvent event;

    @Mock
    User user;

    String args1 = "c";
    String args2 = "f 1";

    @Test
    void testExecuteSingular() {
        when(event.getArgs()).thenReturn(args1);
        when(event.getAuthor()).thenReturn(user);
        when(user.getId()).thenReturn("123456789987654321");
        command.execute(event);
    }

    @Test
    void testExecuteMore() {
        when(event.getArgs()).thenReturn(args2);
        when(event.getAuthor()).thenReturn(user);
        when(user.getId()).thenReturn("123456789987654321");
        command.execute(event);
    }
}