package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InfoCommandTest {

    @InjectMocks
    InfoCommand infoCommand;

    @Mock
    CommandEvent event;

    String args = "b";

    @Test
    void testExecute() {
        when(event.getArgs()).thenReturn(args);
        infoCommand.execute(event);
    }

}