package com.mythden.mythdenbot.parsetranslator.translator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class FacadeTranslatorTest {

    String message;

    String alpha;

    String morse;


    @BeforeEach
    void setUp() throws Exception{
        message = "kenta";
        alpha = "alpha";
        morse = "morse";
    }

    @Test
    void translateArray() throws Exception{
        ArrayList<String> result1 = FacadeTranslator.translateArray(alpha, morse, message);
        assertEquals("-.- . -. - .- ", result1.get(0));
        ArrayList<String> result2 = FacadeTranslator.translateArray(morse, alpha, result1.get(0));
        assertEquals(message, result2.get(0));
    }

    @Test
    void raw() throws Exception{
        ArrayList<String> result1 = FacadeTranslator.raw(alpha, morse, message);
        assertEquals("kenta", result1.get(0));
    }
}