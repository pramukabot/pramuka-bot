package com.mythden.mythdenbot.parsetranslator.translator;

import com.mythden.mythdenbot.parsetranslator.codex.AlphaCodex;
import com.mythden.mythdenbot.parsetranslator.codex.CodexInterface;
import com.mythden.mythdenbot.parsetranslator.codex.MorseCodex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GeneralTranslatorTest {

    @Mock
    GeneralTranslator translator;

    @Mock
    GeneralTranslator translator1;

    @Mock
    CodexInterface alpha;

    @Mock
    CodexInterface morse;


    @BeforeEach
    void setUp() throws Exception{
        alpha = new AlphaCodex();
        morse = new MorseCodex();
        translator = new GeneralTranslator(alpha, morse);
        translator1 = new GeneralTranslator(morse, alpha);
    }

    @Test
    void translate() throws Exception{
        String message = "auki";
        ArrayList<String> message_parsed = translator.translateArray(message);
        assertEquals(".- ..- -.- .. ", message_parsed.get(0));
        ArrayList<String> message_parsed_back = translator1.translateArray(message_parsed.get(0));
        assertEquals(message, message_parsed_back.get(0));
    }
}