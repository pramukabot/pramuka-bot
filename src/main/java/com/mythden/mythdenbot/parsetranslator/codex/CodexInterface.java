package com.mythden.mythdenbot.parsetranslator.codex;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor

public abstract class CodexInterface {
    protected ArrayList<String> CodexList;
    protected String Delimiter;

    public int getIndex(String string){return CodexList.indexOf((string));}
    public String getCodex(int index) {return CodexList.get(index);}
    public String getDelimiter() {return Delimiter;}
}
