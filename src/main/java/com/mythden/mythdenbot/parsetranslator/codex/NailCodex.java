package com.mythden.mythdenbot.parsetranslator.codex;

import java.util.ArrayList;
import java.util.Arrays;

public class NailCodex extends CodexInterface{
    public NailCodex() {
        super(new ArrayList(Arrays.asList(new String[] {} )), "<:nail_base:836101262881325057>");
        Converter converter = new Converter();
        CodexList = converter.initList("nail");
    }
}
