package com.mythden.mythdenbot.parsetranslator.codex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GrassCodex extends CodexInterface{

    public GrassCodex() {
        super(new ArrayList(Arrays.asList(new String[] {} )), "<:grass_base:836069175151165460>");
        Converter converter = new Converter();
        CodexList = converter.initList("grass");
    }
}
