package com.mythden.mythdenbot.parsetranslator.codex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Converter {
    String[] morse_grass = {"<:grass_high:836069291157487666>",
                            "<:grass_low:836069250640510976>"};

    String[] morse_nail = {"<:nail_high:836101262659026956>",
                            "<:nail_low:836101262834532432>"};

    String[] morse_code = {"-", "."};

    String[] morse_raw = {
            ".-","-...","-.-.","-..",".","..-.",
            "--.","....","..",".---","-.-",".-..",
            "--","-.","---",".--.","--.-",".-.",
            "...","-","..-","...-",".--","-..-",
            "-.--","--..","-----",".----","..---",
            "...--","....-",".....","-....","--...",
            "---..","----.",".-.-.-","--..--","..--..",
            ".----.","-.-.--","-..-."};

    private List<String> Grass = Arrays.asList(morse_grass);

    private List<String> Morse = Arrays.asList(morse_code);

    private List<String> Nail = Arrays.asList(morse_nail);

    private final Map<String, List<String>> map = Map.of(
            "grass", Grass,
            "nail", Nail
    );

    public String converter(String message, List<String> codex) {
        StringBuffer buffer = new StringBuffer();
        String[] arr = message.split("");
        for(String s: arr) {
            int index = Morse.indexOf(s);
            String character = codex.get(index);
            buffer.append(character);
        }
        return buffer.toString();
    }

    public ArrayList<String> initList(String codex_source) {
        ArrayList<String> Converted_List = new ArrayList<String>();
        for(String s: morse_raw) {
            String add_to_list = converter(s, getSourceCodex(codex_source));
            Converted_List.add(add_to_list);
        }
        Converted_List.add(" ");
        return Converted_List;
    }

    public List<String> getSourceCodex(String codex_source) {
        return map.get(codex_source);
    }
}
