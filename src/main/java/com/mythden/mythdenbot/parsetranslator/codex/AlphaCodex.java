package com.mythden.mythdenbot.parsetranslator.codex;

import java.util.ArrayList;
import java.util.Arrays;

public class AlphaCodex extends CodexInterface {

    public AlphaCodex() {
        super(new ArrayList(Arrays.asList
                (new String[] {
                "a","b","c","d","e","f",
                "g","h","i","j","k","l",
                "m","n","o","p","q","r",
                "s","t","u","v","w","x",
                "y","z","0","1","2","3",
                "4","5","6","7","8","9",
                ".",",","?","'","!","/",
                " "
        })), "");
    }
}
