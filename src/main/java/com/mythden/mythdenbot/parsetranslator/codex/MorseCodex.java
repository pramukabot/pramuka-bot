package com.mythden.mythdenbot.parsetranslator.codex;

import java.util.ArrayList;
import java.util.Arrays;

public class MorseCodex extends CodexInterface{

    public MorseCodex() {
        super(new ArrayList(Arrays.asList
                (new String[] {
                        ".-","-...","-.-.","-..",".","..-.",
                        "--.","....","..",".---","-.-",".-..",
                        "--","-.","---",".--.","--.-",".-.",
                        "...","-","..-","...-",".--","-..-",
                        "-.--","--..","-----",".----","..---",
                        "...--","....-",".....","-....","--...",
                        "---..","----.",".-.-.-","--..--","..--..",
                        ".----.","-.-.--","-..-.","\n"
                })), " ");
    }
}
