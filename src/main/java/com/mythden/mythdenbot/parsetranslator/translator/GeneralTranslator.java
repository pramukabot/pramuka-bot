package com.mythden.mythdenbot.parsetranslator.translator;

import com.mythden.mythdenbot.parsetranslator.codex.CodexInterface;

import java.util.ArrayList;
import java.util.Arrays;

public class GeneralTranslator implements TranslatorInterface{
    private CodexInterface codex_source;
    private CodexInterface codex_target;

    public GeneralTranslator(CodexInterface codex_source, CodexInterface codex_target) {
        this.codex_source = codex_source;
        this.codex_target = codex_target;
    }

    public ArrayList<String> translateArray(String message) {
        ArrayList<String> arr = new ArrayList<String>();
        message = message.toLowerCase();
        String[] arrayMessage = message.split(codex_source.getCodex(42));
        for(String s: arrayMessage) {
            StringBuffer text = new StringBuffer();
            String[] arrayMessage1 = s.split(codex_source.getDelimiter());
            for(String x: arrayMessage1) {
                int index = codex_source.getIndex(x);
                String character = codex_target.getCodex(index);
                text.append(character);
                text.append(codex_target.getDelimiter());
            }
            arr.add(text.toString());
        }
        return arr;
    }

    public ArrayList<String> returnRaw(String message) {
        ArrayList<String> arr = new ArrayList<String>();
        String[] arrayMessage = message.split(codex_source.getCodex(42));
        for(String s: arrayMessage) {
            arr.add(s);
        }
        return arr;
    }
}
