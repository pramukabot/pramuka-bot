package com.mythden.mythdenbot.parsetranslator.translator;

import com.mythden.mythdenbot.parsetranslator.codex.*;

import java.util.ArrayList;
import java.util.Map;

public class FacadeTranslator {
    private static final CodexInterface alpha_codex = new AlphaCodex();
    private static final CodexInterface morse_codex = new MorseCodex();
    private static final CodexInterface grass_codex = new GrassCodex();
    private static final CodexInterface nail_codex = new NailCodex();
    private static final CodexInterface box_codex = new BoxCodex();

    private static final Map<String, CodexInterface> map = Map.of(
            "alpha", alpha_codex,
            "morse", morse_codex,
            "grass", grass_codex,
            "nail", nail_codex,
            "box", box_codex);


    public static ArrayList<String> translateArray(String source, String target, String message) {
        GeneralTranslator generalTranslator = new GeneralTranslator(map.get(source), map.get(target));
        return generalTranslator.translateArray(message);
    }

    public static ArrayList<String> raw(String source, String target, String message) {
        GeneralTranslator generalTranslator = new GeneralTranslator(map.get(source), map.get(target));
        return generalTranslator.returnRaw(message);
    }
}
