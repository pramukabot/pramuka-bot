package com.mythden.mythdenbot.parsetranslator.translator;

import java.util.ArrayList;

public interface TranslatorInterface {
    ArrayList<String> translateArray(String message);
    ArrayList<String> returnRaw(String message);
}
