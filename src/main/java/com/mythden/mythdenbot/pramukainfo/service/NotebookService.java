package com.mythden.mythdenbot.pramukainfo.service;

import com.mythden.mythdenbot.pramukainfo.model.Info;
import com.mythden.mythdenbot.pramukainfo.model.Notebook;

public interface NotebookService {
    Notebook saveNotebook(Notebook notebook);

    Notebook addInfo(Info info, String owner);
    Notebook removeInfo(Integer index, String owner);

    Notebook getNotebookByOwner(String owner);
    void deleteNotebookByOwner(String owner);
}
