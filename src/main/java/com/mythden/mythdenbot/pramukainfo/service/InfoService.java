package com.mythden.mythdenbot.pramukainfo.service;

import com.mythden.mythdenbot.pramukainfo.model.Info;

public interface InfoService {
    Info createInfo(Info info);
    Info updateInfo(Integer id, Info info);
    void deleteInfo(Integer id);

    Info getInfoByTitle(String title);
    Info getInfoById(Integer id);
    Iterable<Info> getAllInfo();
}
