package com.mythden.mythdenbot.pramukainfo.service;

import java.util.List;

import javax.transaction.Transactional;

import com.mythden.mythdenbot.pramukainfo.model.Info;
import com.mythden.mythdenbot.pramukainfo.model.Notebook;
import com.mythden.mythdenbot.pramukainfo.repository.NotebookRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class NotebookServiceImpl implements NotebookService {

    @Autowired
    public NotebookRepository notebookRepository;

    @Override
    public Notebook saveNotebook(Notebook notebook) {
        return notebookRepository.save(notebook);
    }

    @Override
    public Notebook addInfo(Info info, String owner) {
        Notebook notebook = getNotebookByOwner(owner);
        List<Info> infos = notebook.getInfos();
        if (infos.contains(info)) throw new IllegalArgumentException();
        infos.add(info);
        return notebookRepository.save(notebook);
    }

    @Override
    public Notebook removeInfo(Integer index, String owner) {
        Notebook notebook = getNotebookByOwner(owner);
        List<Info> infos = notebook.getInfos();
        infos.remove(index.intValue());
        return notebookRepository.save(notebook);
    }

    @Override
    public Notebook getNotebookByOwner(String owner) {
        return notebookRepository.findByOwner(owner);
    }

    @Override
    public void deleteNotebookByOwner(String owner) {
        Notebook notebook = getNotebookByOwner(owner);
        notebookRepository.delete(notebook);
    }
    
}
