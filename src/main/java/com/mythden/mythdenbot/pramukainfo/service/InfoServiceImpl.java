package com.mythden.mythdenbot.pramukainfo.service;

import com.mythden.mythdenbot.pramukainfo.model.Info;
import com.mythden.mythdenbot.pramukainfo.repository.InfoRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class InfoServiceImpl implements InfoService {

    @Autowired
    public InfoRepository infoRepository;

    @Override
    public Info createInfo(Info info) {
        return infoRepository.save(info);
    }

    @Override
    public Info updateInfo(Integer id, Info info) {
        info.setIdInfo(id);
        return infoRepository.save(info);
    }

    @Override
    public void deleteInfo(Integer id) {
        Info info = getInfoById(id);
        infoRepository.delete(info);
    }

    @Override
    public Info getInfoByTitle(String title) {
        return infoRepository.findByTitleContainingIgnoreCase(title);
    }

    @Override
    public Info getInfoById(Integer id) {
        return infoRepository.findByIdInfo(id);
    }

    
    @Override
    public Iterable<Info> getAllInfo() {
        return infoRepository.findAllByOrderByIdInfoAsc();
    }
    
}
