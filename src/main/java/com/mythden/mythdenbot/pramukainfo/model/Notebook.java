package com.mythden.mythdenbot.pramukainfo.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import static com.mythden.mythdenbot.pramukainfo.core.ParserHelper.getUsername;

import lombok.Data;
import lombok.NoArgsConstructor;
@Entity
@Table(name = "notebook")
@Data
@NoArgsConstructor
public class Notebook implements IListable {

    @Id
    @Column(name = "owner", nullable = false, updatable = false)
    private String owner;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "info")
    private List<Info> infos = new ArrayList<>();

    @Override
    public String getListingTitle() {
        return getUsername(this.owner);
    }

    @Override
    public int getListingId() {
        return 0;
    }

    
    
}
