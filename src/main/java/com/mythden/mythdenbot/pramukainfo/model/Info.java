package com.mythden.mythdenbot.pramukainfo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "info")
@Getter
@Setter
@NoArgsConstructor
public class Info implements IListable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_info", updatable = false, nullable = false)
    private Integer idInfo;

    @Column(name = "title")
    private String title;

    @Lob
    @Column(name = "content")
    private String content;

    @Override
    public String getListingTitle() {
        return this.title;
    }

    @Override
    public int getListingId() {
        return this.idInfo;
    }

    @Override
    public boolean equals(Object obj) {
        return this.idInfo.intValue() == ((Info) obj).getIdInfo().intValue();
    }


}
