package com.mythden.mythdenbot.pramukainfo.model;

public interface IListable {
    String getListingTitle();
    int getListingId();
}
