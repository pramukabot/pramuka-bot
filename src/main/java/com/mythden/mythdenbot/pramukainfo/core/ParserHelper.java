package com.mythden.mythdenbot.pramukainfo.core;

import static com.mythden.mythdenbot.PramukaBot.jda;

public class ParserHelper {

    public static String getUsername(String id) {
        return jda.retrieveUserById(id).complete().getName();
    }
}
