package com.mythden.mythdenbot.pramukainfo.core.module;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class Module<T> implements Cloneable {
    private boolean init;

    public List<Object> args = new ArrayList<>();

    public void addArgs(Object[] objectArgs) {
        assert init : "Module object not created via create()";
        this.flush();
        int i = 0;
        while (i < params()) {
            this.args.add(objectArgs[i]);
            i++;
        }
    }

    private void flush() {
        this.args.clear();
    }

    public T invoke() {
        try {
            T product = produce();
            this.flush();
            return product;
        } catch (Exception e) {
            this.flush();
            System.out.println(Arrays.toString(e.getStackTrace()));
            return null;
        }
    }

    protected abstract T produce();
    public abstract int params();

    @SuppressWarnings("unchecked")
    public Module<T> create() {
        Module<T> module = null;
        try {
            module = (Module<T>) this.clone();
            module.init = true;
        } catch (CloneNotSupportedException ignored) {}
        return module;
    }
}