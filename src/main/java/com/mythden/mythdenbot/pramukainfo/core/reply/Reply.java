package com.mythden.mythdenbot.pramukainfo.core.reply;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;

public abstract class Reply {

    protected final EmbedBuilder builder = new EmbedBuilder();

    private void process() {
        builder.setColor(0x36FF78);
        builder.setTitle(getTitle());
        builder.setDescription(getContent());
        addFooter();
    }

    protected abstract String getTitle();
    protected abstract String getContent();
    protected abstract void addFooter();

    public Message get() {
        process();
        Message msg = new MessageBuilder(builder).build();
        return msg;
    }

    
}
