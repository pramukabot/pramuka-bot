package com.mythden.mythdenbot.pramukainfo.core.module;

import com.mythden.mythdenbot.pramukainfo.model.Info;

public class ModuleHelper {

    public static String parseString(Object stringObject) {
        return (String) stringObject;
    }

    public static String parseUserId(Object stringObject) {
        String string = parseString(stringObject);
        return string.substring(0, 18);
    }
    
    public static int parseInt(Object stringObject) {
        String str = parseString(stringObject);
        return Integer.parseInt(str);
    }

    public static String[] parseInfoContent(Object stringObject) {
        return parseString(stringObject).split("\n", 2);
    }

    public static Info parseToInfo(Object stringObject) {
        Info info = new Info();
        String[] content = parseInfoContent(stringObject);
        info.setTitle(content[0]);
        info.setContent(content[1]);
        return info;
    }

}
