package com.mythden.mythdenbot.pramukainfo.core.reply;

import com.mythden.mythdenbot.pramukainfo.model.Notebook;
import static com.mythden.mythdenbot.pramukainfo.core.reply.ReplyHelper.getRepliesFromInfos;

public class ReplyNotebook extends ReplyPaged {

    private final Notebook nb;

    public ReplyNotebook(Notebook notebook) {
        super(getRepliesFromInfos(notebook.getInfos()), notebook.getListingTitle());
        this.nb = notebook;
    }

    @Override
    protected void addFooter() {
        builder.setFooter("ID: "+nb.getOwner());
    }


    
}
