package com.mythden.mythdenbot.pramukainfo.core.reply;

import com.mythden.mythdenbot.pramukainfo.core.reply.ListingHelper.ListType;
import com.mythden.mythdenbot.pramukainfo.model.Info;
import com.mythden.mythdenbot.pramukainfo.model.Notebook;

import java.util.ArrayList;
import java.util.List;

public class ReplyHelper {

    public static final ReplyMock DELETE = new ReplyMock("DECIMATOR");
    public static final ReplyMock GET = new ReplyMock("RETRIEVER");
    public static final ReplyMock CREATE = new ReplyMock("FORMGIVER");
    public static final ReplyMock UPDATE = new ReplyMock("REVAMPER");
    public static final ReplyMock DEBUG = new ReplyMock("HARBINGER");
    public static final ReplyMock TIMER = new ReplyMock("TIMEKEEPER");
    public static final ReplyMock NULL = new ReplyMock("???").with("You're staring at the depths of the abyss...");

    public static ReplyList getNotebookTOC(Notebook nb) {
        return new ReplyList(ListType.INDEX, nb.getListingTitle(), nb.getInfos());
    }

    public static List<Reply> getRepliesFromInfos(List<Info> infos) {
        List<Reply> replies = new ArrayList<>();
        infos.forEach((info) -> {
            replies.add(new ReplyInfo(info));
        });
        return replies;
    }

    public static List<Reply> of(Reply ... replies) {
        return List.of(replies);
    }
    
    public static List<Reply> getNull() {
        return of(DEBUG.with("Whoa! what's this sad, empty giftbox?"));
    }

    public static List<Reply> getDebug(String debug) {
        return of(DEBUG.with(debug));
    }

    public static ReplyMock getTimerReply(Long milis) {
        return TIMER.with("Elapsed time: "+milis+"ms");
    }

}