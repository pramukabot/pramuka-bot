package com.mythden.mythdenbot.pramukainfo.core.reaction;

import com.mythden.mythdenbot.pramukainfo.core.CommandMap;
import com.mythden.mythdenbot.pramukainfo.core.CommandParser;
import com.mythden.mythdenbot.pramukainfo.core.reply.Reply;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.List;

public class ReactionHandler extends ListenerAdapter {
    
    @Override
    public void onGenericMessageReaction(GenericMessageReactionEvent event) {
        if (isMessageFromSelf(event)) {
            if (isRightReaction(event)) {
                if (isInfo(event)) {
                    if (isReactionNotFromBot(event)) {
                        String infoTitle = getTitle(event);
                        String reactor = event.getUserId();
                        String[] wholeArgs = new String[]{"ai", reactor + " " + infoTitle};

                        List<Reply> replies = CommandParser.getReply(wholeArgs, CommandMap.NOTEBOOK_COMMANDS);
                        replies.forEach((reply) -> {
                            event.getChannel().sendMessage(reply.get()).complete();
                        });
                    }
                }
            }
        }

    }

    private static boolean isRightReaction(GenericMessageReactionEvent event) {
        return event.getReactionEmote().getName().equals("📝");
    }

    private static boolean isInfo(GenericMessageReactionEvent event) {
        Message msg = getMessageFromEvent(event);
        return getEmbedFooterFromMessage(msg).startsWith("ID: ");
    }

    private static boolean isMessageFromSelf(GenericMessageReactionEvent event) {
        Message message = getMessageFromEvent(event);
        return message.getAuthor().equals(getBot(event));
    }

    private static boolean isReactionNotFromBot(GenericMessageReactionEvent event) {
        return !event.getUserId().equals(getBot(event).getId());
    }

    private static User getBot(GenericMessageReactionEvent event) {
        return event.getJDA().getSelfUser();
    }

    private static Message getMessageFromEvent(GenericMessageReactionEvent event) {
        return event.getChannel().retrieveMessageById(event.getMessageIdLong()).complete();
    }

    private static String getEmbedFooterFromMessage(Message msg) {
        return msg.getEmbeds().get(0).getFooter().getText();
    }

    private static String getEmbedTitleFromMessage(Message msg) {
        return msg.getEmbeds().get(0).getTitle();
    }

    private static String getTitle(GenericMessageReactionEvent event) {
        return getEmbedTitleFromMessage(getMessageFromEvent(event));
    }

}