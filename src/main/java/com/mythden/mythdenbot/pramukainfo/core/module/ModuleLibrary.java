package com.mythden.mythdenbot.pramukainfo.core.module;

import com.mythden.mythdenbot.pramukainfo.core.reply.ListingHelper.ListType;
import com.mythden.mythdenbot.pramukainfo.core.reply.Reply;
import com.mythden.mythdenbot.pramukainfo.core.reply.ReplyInfo;
import com.mythden.mythdenbot.pramukainfo.core.reply.ReplyList;
import com.mythden.mythdenbot.pramukainfo.core.reply.ReplyNotebook;
import com.mythden.mythdenbot.pramukainfo.model.Info;
import com.mythden.mythdenbot.pramukainfo.model.Notebook;
import com.mythden.mythdenbot.pramukainfo.service.InfoService;
import com.mythden.mythdenbot.pramukainfo.service.NotebookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.List;

import static com.mythden.mythdenbot.pramukainfo.core.module.ModuleHelper.*;
import static com.mythden.mythdenbot.pramukainfo.core.reply.ReplyHelper.*;

@Component
public class ModuleLibrary {

    private static InfoService infoService;
    private static NotebookService notebookService;

    @Autowired
    public void setServices(InfoService infoService, NotebookService notebookService) {
        ModuleLibrary.infoService = infoService;
        ModuleLibrary.notebookService = notebookService;
    }

    /**
     * Consumes 1 arguments
     * <ul>
     * <li>{@code String title and content}</li>
     * </ul>
     */
    public static final Module<List<Reply>> CREATE_INFO = new Module<>() {
        @Override
        public List<Reply> produce() {
            Info info = parseToInfo(args.get(0));
            info = infoService.createInfo(info);
            return of(CREATE.success(), new ReplyInfo(info));
        }

        @Override
        public int params() {
            return 1;
        }
    };

    /**
     * Consumes 2 arguments
     * <ul>
     * <li>{@code Integer info id to override}</li>
     * <li>{@code String title and content}</li>
     * </ul>
     */
    public static final Module<List<Reply>> UPDATE_INFO = new Module<>() {
        @Override
        public List<Reply> produce() {
            int id = parseInt(args.get(0));
            Info info = parseToInfo(args.get(1));
            info = infoService.updateInfo(id, info);
            return of(UPDATE.success(), new ReplyInfo(info));
        }

        @Override
        public int params() {
            return 2;
        }
    };

    /**
     * Consumes 1 argument
     * <ul>
     * <li>{@code Integer info id to delete}</li>
     * </ul>
     */
    public static final Module<List<Reply>> DELETE_INFO = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                int id = parseInt(args.get(0));
                infoService.deleteInfo(id);
                return of(DELETE.success());
            } catch (Exception e) {
                return of(DELETE.notFound());
            }
        }

        @Override
        public int params() {
            return 1;
        }
    };

    /**
     * Consumes 1 argument
     * <ul>
     * <li>{@code Integer info id to get}</li>
     * </ul>
     */
    public static final Module<List<Reply>> GET_INFO_BY_ID = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                int id = parseInt(args.get(0));
                Info info = infoService.getInfoById(id);
                if (info == null) throw new EntityNotFoundException();
                return of(GET.success(), new ReplyInfo(info));
            } catch (Exception e) {
                return of(GET.notFound());
            }
        }

        @Override
        public int params() {
            return 1;
        }
    };

    /**
     * Consumes 1 argument
     * <ul>
     * <li>{@code String title}</li>
     * </ul>
     */
    public static final Module<List<Reply>> GET_INFO_BY_TITLE = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                String title = parseString(args.get(0));
                Info info = infoService.getInfoByTitle(title);
                if (info == null) throw new Exception();
                return of(GET.success(), new ReplyInfo(info));
            } catch (Exception e) {
                return of(GET.notFound());
            }
        }

        @Override
        public int params() {
            return 1;
        }
    };

    /**
     * Consumes no argument
     */
    public static final Module<List<Reply>> GET_ALL_INFO = new Module<>() {
        @Override
        public List<Reply> produce() {
            return of(GET.success(), new ReplyList(ListType.ID, "All available info", infoService.getAllInfo()));
        }

        @Override
        public int params() {
            return 0;
        }
    };

    /**
     * Consumes 1 argument, the userId
     */
    public static final Module<List<Reply>> CREATE_NOTEBOOK = new Module<>() {
        @Override
        public List<Reply> produce() {
            String user = parseUserId(args.get(0));

            Notebook nb = notebookService.getNotebookByOwner(user);
            if (nb != null) return of(CREATE.with("Thou shall not overwrite thy old scriptures!"));

            nb = new Notebook();
            nb.setOwner(user);
            nb = notebookService.saveNotebook(nb);

            return of(CREATE.success(), new ReplyNotebook(nb));
        }

        @Override
        public int params() {
            return 1;
        }
    };

    /**
     * Consumes 1 argument, the userId
     */
    public static final Module<List<Reply>> DELETE_NOTEBOOK = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                String user = parseUserId(args.get(0));
                notebookService.deleteNotebookByOwner(user);
                return of(DELETE.success());
            } catch (Exception e) {
                return of(DELETE.notFound());
            }
        }

        @Override
        public int params() {
            return 1;
        }
    };

    /**
     * Consumes 2 arguments, the userId and the info's title
     */
    public static final Module<List<Reply>> ADD_INSTANT_TO_NOTEBOOK = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                String user = parseUserId(args.get(0));
                String title = parseString(args.get(1));
                Info info = infoService.getInfoByTitle(title);
                Notebook nb = notebookService.addInfo(info, user);
                return of(UPDATE.success(), getNotebookTOC(nb));
            } catch (IllegalArgumentException e ) {
                return of(UPDATE.with("What is this contraband?"));
            } catch (Exception e) {
                return of(UPDATE.with("Thy call hadth sundered the fabric of reality..."));
            }
        }

        @Override
        public int params() {
            return 2;
        }
    };

    /**
     * Consumes 1 argument, the userId
     */
    public static final Module<List<Reply>> GET_TOC = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                String user = parseUserId(args.get(0));
                Notebook nb = notebookService.getNotebookByOwner(user);
                return of(GET.success(), new ReplyList(ListType.INDEX, nb.getListingTitle(), nb.getInfos()));
            } catch (Exception e) {
                return of(GET.notFound());
            }
        }

        @Override
        public int params() {
            return 1;
        }
    };

    /**
     * Consumes 2 arguments, the userId and the page index to be deleted
     */
    public static final Module<List<Reply>> DELETE_PAGE = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                String user = parseUserId(args.get(0));
                int index = parseInt(args.get(1));
                Notebook nb = notebookService.removeInfo(index-1, user);
                return of(UPDATE.success(), getNotebookTOC(nb));
            } catch (IndexOutOfBoundsException e) {
                return of(UPDATE.indexOutOfBounds());
            } catch (Exception e) {
                return of(UPDATE.notFound());
            }
        }

        @Override
        public int params() {
            return 2;
        }
    };

    /**
     * Consumes 2 arguments, the userId and the page index to get
     */
    public static final Module<List<Reply>> GET_PAGE = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                String user = parseUserId(args.get(0));
                int index = parseInt(args.get(1));
                Notebook nb = notebookService.getNotebookByOwner(user);
                Info info = nb.getInfos().get(index-1);
                return of(GET.success(), new ReplyInfo(info));
            } catch (IndexOutOfBoundsException e) {
                return of(GET.indexOutOfBounds());
            } catch (Exception e) {
                return of(GET.notFound());
            }
        }

        @Override
        public int params() {
            return 2;
        }
    };

    /**
     * Consumes 1 arguments the userId
     */
    public static final Module<List<Reply>> OPEN_NOTEBOOK = new Module<>() {
        @Override
        public List<Reply> produce() {
            try {
                String user = parseUserId(args.get(0));
                Notebook nb = notebookService.getNotebookByOwner(user);
                return of(GET.success(), new ReplyNotebook(nb));
            } catch (Exception e) {
                return of(GET.notFound());
            }
        }

        @Override
        public int params() {
            return 1;
        }
    };

}
