package com.mythden.mythdenbot.pramukainfo.core.reply;

import java.util.ArrayList;
import java.util.List;

import static com.mythden.mythdenbot.pramukainfo.core.reply.ReplyHelper.NULL;

import com.github.ygimenez.model.Page;
import com.github.ygimenez.type.PageType;

import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class ReplyPaged extends Reply {
    @Setter
    protected List<Reply> replies = new ArrayList<>();
    private String title = "";


    public ReplyPaged(List<Reply> replies, String title) {
        this.replies = replies;
        this.title = title;
    }

    public ArrayList<Page> getPages() {
        ArrayList<Page> pages = new ArrayList<>();
        replies.forEach((reply) -> {
            pages.add(new Page(PageType.TEXT, reply.get()));
        });
        if (pages.size() == 0) pages.add(new Page(PageType.TEXT, NULL.get()));
        return pages;
    }

    @Override
    protected String getTitle() {
        return this.title;
    }

    @Override
    protected String getContent() {
        return "";
    }

    @Override
    protected void addFooter() {}
    
}
