package com.mythden.mythdenbot.pramukainfo.core;


import com.mythden.mythdenbot.pramukainfo.core.module.Module;
import com.mythden.mythdenbot.pramukainfo.core.reply.Reply;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

import static com.mythden.mythdenbot.pramukainfo.core.reply.ReplyHelper.getDebug;
import static com.mythden.mythdenbot.pramukainfo.core.reply.ReplyHelper.getNull;
import static com.mythden.mythdenbot.pramukainfo.core.reply.ReplyHelper.getTimerReply;
public class CommandParser {

    private static final List<String> NO_ARGS = List.of("a");

    public static List<Reply> getReply(String[] wholeArgs, CommandMap map) {
        String command = wholeArgs[0];
        Module<List<Reply>> module = map.get(command);

        if (module == null) return getDebug("We cannot comprehend your request!");
        module = module.create();

        try {
            if (!NO_ARGS.contains(command)) {
                getObjectArrayForModule(wholeArgs[1], module);
            }
            return processModule(module);
        }  catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            return getDebug("The slimes jammed the module-inator... again...");
        }
    }

    private static void getObjectArrayForModule(String stringArgs, Module<?> module) {
        Object[] objectArgs = stringArgs.split(" ", module.params());
        module.addArgs(objectArgs);
    }

    private static List<Reply> processModule(Module<List<Reply>> module) throws ExecutionException, InterruptedException {
        CompletableFuture<List<Reply>> cf = new CompletableFuture<>();

        long time = Executors.newCachedThreadPool().submit(() -> {
            long start = System.currentTimeMillis();
            List<Reply> result = module.invoke();
            if (result == null) result = getNull();
            cf.complete(result);
            long stop = System.currentTimeMillis();
            return stop - start;
        }).get();

        List<Reply> res = cf.get();
        List<Reply> fin = new ArrayList<>(res);
        fin.add(getTimerReply(time));
        return fin;
    }

}
