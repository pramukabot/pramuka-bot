package com.mythden.mythdenbot.pramukainfo.core.reply;

import java.util.ArrayList;
import java.util.List;

import com.mythden.mythdenbot.pramukainfo.model.IListable;

import static com.mythden.mythdenbot.pramukainfo.core.reply.ListingHelper.*;

import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class ReplyList extends ReplyPaged {
    private String content = "";

    @Setter
    private String title;

    public ReplyList(ListType type, String title, Iterable<? extends IListable> listables) {
        this.title = title;
        this.content = type.runListingMethod(new Object[]{listables});
        setReplies(this.paginate());
    }

    protected void addContent(String str) {
        this.content += "\n" + str;
    }

    private List<Reply> paginate() {
        String[] contentSplit = content.split("\n");
        List<Reply> pages = new ArrayList<>();

        int patron = contentSplit.length;
        for (int i = 0; i < patron;) {
            ReplyList reply = new ReplyList();
            reply.setTitle(this.title);
            for (int j = 0; j < 15; j++) {
                reply.addContent(contentSplit[i]);
                i++;
                if (i == patron) break;
            }
            pages.add(reply);
        }
        return pages;
    }

    @Override
    protected String getTitle() {
        return title;
    }

    @Override
    protected String getContent() {
        return content;
    }

}
