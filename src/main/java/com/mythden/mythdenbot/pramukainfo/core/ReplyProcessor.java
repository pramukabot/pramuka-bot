package com.mythden.mythdenbot.pramukainfo.core;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Page;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.pramukainfo.core.reply.Reply;
import com.mythden.mythdenbot.pramukainfo.core.reply.ReplyInfo;
import com.mythden.mythdenbot.pramukainfo.core.reply.ReplyPaged;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;

import java.util.ArrayList;
import java.util.List;

public class ReplyProcessor {
    public static void produceReply(CommandEvent event, List<Reply> replies) {
        replies.forEach((reply) -> {
            Message msg = reply.get();
            if (reply instanceof ReplyPaged) {
                ArrayList<Page> pages = ((ReplyPaged) reply).getPages();
                MessageChannel channel = event.getChannel();
                MessageAction action = channel.sendMessage((Message) pages.get(0).getContent());
                action.queue(success -> {
                    if (pages.size() > 1)
                        Pages.paginate(success, pages);
                });
            } else if (reply instanceof ReplyInfo) {
                event.getChannel().sendMessage(msg).complete().addReaction("📝").queue();
            } else {
                event.reply(msg);
            }
        });
    }
}
