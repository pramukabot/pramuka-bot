package com.mythden.mythdenbot.pramukainfo.core;

import java.util.List;
import java.util.Map;

import static com.mythden.mythdenbot.pramukainfo.core.module.ModuleLibrary.*;
import com.mythden.mythdenbot.pramukainfo.core.module.Module;
import com.mythden.mythdenbot.pramukainfo.core.reply.Reply;

public class CommandMap {

    private final Map<String, Module<List<Reply>>> MAP;

    public static final CommandMap INFO_COMMANDS = new CommandMap(Map.of(
        "c", CREATE_INFO,
        "u", UPDATE_INFO,
        "d", DELETE_INFO,
        "f", GET_INFO_BY_ID,
        "s", GET_INFO_BY_TITLE,
        "a", GET_ALL_INFO
    ));

    public static final CommandMap NOTEBOOK_COMMANDS = new CommandMap(Map.of(
        "c", CREATE_NOTEBOOK,
        "o", OPEN_NOTEBOOK,
        "d", DELETE_NOTEBOOK,
        "ai", ADD_INSTANT_TO_NOTEBOOK,
        "toc", GET_TOC,
        "f", GET_PAGE,
        "rm", DELETE_PAGE
    ));

    private CommandMap(Map<String, Module<List<Reply>>> map) {
        this.MAP = map;
    }

    public Module<List<Reply>> get(String key) {
        return MAP.get(key);
    }
}
