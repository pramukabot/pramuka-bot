package com.mythden.mythdenbot.pramukainfo.core.reply;

import com.mythden.mythdenbot.pramukainfo.core.module.Module;
import com.mythden.mythdenbot.pramukainfo.model.IListable;

import java.util.List;

public class ListingHelper {

    public static String getDigitSpacing(Integer id) {
        String str = id.toString();
        return " ".repeat(2 - str.length());
    }

    public static String getNumbering(int num) {
        return String.format("`%s%s` ", getDigitSpacing(num), num);
    }

    @SuppressWarnings("unchecked")
    public enum ListType {
        ID (new Module<String>() {
            @Override
            protected String produce() {
                Iterable<IListable> listables = (Iterable<IListable>) args.get(0);
                StringBuilder str = new StringBuilder();
                listables.forEach((listable) -> {
                    str.append("\n" + getNumbering(listable.getListingId()) + listable.getListingTitle());
                });
                return str.toString();
            }
            @Override
            public int params() {return 1;}
        }.create()),
        INDEX (new Module<String>() {
            @Override
            protected String produce() {
                List<IListable> listables = (List<IListable>) args.get(0);
                StringBuilder str = new StringBuilder();
                for (int i = 0; i < listables.size(); i++){
                    str.append("\n" + getNumbering(i+1) + listables.get(i).getListingTitle());
                }
                return str.toString();
            }
            @Override
            public int params() {return 1;}
        }.create());

        private final Module<String> MODULE;

        private ListType(Module<String> module) {
            this.MODULE = module;
        }

        public String runListingMethod(Object[] objectArgs) {
            try {
                this.MODULE.addArgs(objectArgs);
                return this.MODULE.invoke();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
