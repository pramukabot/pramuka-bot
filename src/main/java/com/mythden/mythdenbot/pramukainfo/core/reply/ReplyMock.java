package com.mythden.mythdenbot.pramukainfo.core.reply;

import lombok.Setter;

public class ReplyMock extends Reply {
    @Setter
    private String debug;
    private String predicate;

    public ReplyMock(String predicate) {
        this.predicate = predicate;
    }

    public ReplyMock with(String debug) {
        setDebug(debug);
        return this;
    }

    public ReplyMock success() {
        return with("Operation successful!");
    }

    public ReplyMock notFound() {
        return with("Hmmm... Can't find anything!");
    }

    public ReplyMock indexOutOfBounds() {
        return with("Which page was that?");
    }    

    @Override
    protected String getTitle() {
        return String.format("```%s```", this.predicate);
    }

    @Override
    protected String getContent() {
        return debug;
    }

    @Override
    protected void addFooter() {}
    
}
