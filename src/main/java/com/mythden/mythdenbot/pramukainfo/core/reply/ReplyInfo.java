package com.mythden.mythdenbot.pramukainfo.core.reply;

import com.mythden.mythdenbot.pramukainfo.model.Info;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ReplyInfo extends Reply {
    private final Info info;

    @Override
    protected String getTitle() {
        return info.getTitle();
    }

    @Override
    protected String getContent() {
        String content = info.getContent();
        getImageFromContent(content);
        return content;
    }

    @Override
    protected void addFooter() {
        builder.setFooter("ID: "+info.getIdInfo());
    }

    private void getImageFromContent(String content) {
        try {
            String res = getImageSubstring(content);
            if (!res.isEmpty()) builder.setImage(res);
        } catch (StringIndexOutOfBoundsException ignored) {}
    }

    private String getImageSubstring(String content) {
        return content.substring(content.indexOf("img(")+4, content.indexOf(")"));
    }

}
