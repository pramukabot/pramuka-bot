package com.mythden.mythdenbot.pramukainfo.repository;

import java.util.List;

import com.mythden.mythdenbot.pramukainfo.model.Info;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InfoRepository extends JpaRepository<Info, Integer>{
    Info findByIdInfo(Integer idInfo);
    List<Info> findAllByOrderByIdInfoAsc();
    Info findByTitleContainingIgnoreCase(String title);
}

