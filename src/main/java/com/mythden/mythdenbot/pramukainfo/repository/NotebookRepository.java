package com.mythden.mythdenbot.pramukainfo.repository;

import com.mythden.mythdenbot.pramukainfo.model.Notebook;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotebookRepository extends JpaRepository<Notebook, String>{
    Notebook findByOwner(String owner);
}

