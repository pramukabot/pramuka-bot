package com.mythden.mythdenbot;

import com.github.ygimenez.exception.InvalidHandlerException;
import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Paginator;
import com.github.ygimenez.model.PaginatorBuilder;
import com.jagrosh.jdautilities.command.CommandClientBuilder;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.jagrosh.jdautilities.examples.command.AboutCommand;
import com.jagrosh.jdautilities.examples.command.PingCommand;
import com.jagrosh.jdautilities.examples.command.ShutdownCommand;
import com.mythden.mythdenbot.commands.*;
import com.mythden.mythdenbot.commands.music.*;
import com.mythden.mythdenbot.pramukainfo.core.reaction.ReactionHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.security.auth.login.LoginException;
import java.awt.*;
import java.io.IOException;


@SpringBootApplication
public class PramukaBot {

    private static String TOKEN;

    private static String OWNER_ID;

    public static JDA jda;

    @Autowired
    public PramukaBot(@Value("${discord.token}") String token,
                      @Value("${discord.owner_id}") String ownerId) {
        TOKEN = token;
        OWNER_ID = ownerId;
    }

    public static void main(String[] args) throws IllegalArgumentException {
        SpringApplication app = new SpringApplication(PramukaBot.class);
        app.run();
    }

    @PostConstruct
    public void run() throws IOException, LoginException,
            IllegalArgumentException, RateLimitedException, InvalidHandlerException {

        EventWaiter waiter = new EventWaiter();
        ReactionHandler reaction = new ReactionHandler();
        CommandClientBuilder client = new CommandClientBuilder();
        // The default is "Type -help" (or whatver prefix you set)
        client.useDefaultGame();
        // sets the owner of the bot
        client.setOwnerId(OWNER_ID);
        // sets emojis used throughout the bot on successes, warnings, and failures
        client.setEmojis("\uD83D\uDE03", "\uD83D\uDE2E", "\uD83D\uDE26");
        // sets the bot prefix
        client.setPrefix("ax-");

        // adds commands
        client.addCommands(
                // command to show information about the bot
                new AboutCommand(Color.BLUE, "an example bot",
                        new String[]{"Cool commands","Nice examples","Lots of fun!"},
                        Permission.ADMINISTRATOR),
                // command to check bot latency
                new PingCommand(),
                // command to shut off the bot
                new ShutdownCommand(),
                // cipher translation command
                new ParseCommand(),

                new InfoCommand(),
                new NotebookCommand(),

                new MiniGamesCommand(),

                new PertanyaanCommand(),

                new JawabCommand(),

                new EndCommand(),
                new JoinCommand(),
                new PlayMusicCommand(),
                new StopCommand(),
                new SkipCommand(),
                new NowPlayingCommand(),
                new QueueCommand(),
                new RepeatCommand(),
                new LeaveCommand(),

                new EightBallCommand()
        );


        // start getting a bot account set up
        jda = JDABuilder.createDefault(TOKEN)
                // set the game for when the bot is loading
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .setActivity(Activity.playing("loading..."))
                // add the listeners
                .addEventListeners(waiter, client.build(), reaction)
                // start it up!
                .build();
        
        // Paginator
        Paginator paginator = PaginatorBuilder.createPaginator()
                .setHandler(jda)
                .shouldRemoveOnReact(true)
                .build();
        Pages.activate(paginator);
    }
}
