package com.mythden.mythdenbot.jsonparser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;

public class JsonParser {

    private static JSONArray arrayPemain = new JSONArray();

    private static HashMap<String, Integer> pemainSkorMap = new HashMap<>();

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            if (jsonText.contains("[")) {
                jsonText = jsonText.replace("[", "").replace("]", "");
            } else if (!jsonText.contains("{")) {
                jsonText = jsonText.replace("[", "{").replace("]", "}");
            }
            System.out.println(jsonText);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    public static HashMap<String, Integer> readJsonFromUrlToArray(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            if (jsonText.contains("[")) {
                jsonText = jsonText.replace("[", "").replace("]", "");
            }
            String[] jsonArray = jsonText.split(",");
            System.out.println(jsonArray.toString());
            for (int i = 0; i < jsonArray.length; i++) {
                jsonArray[i] = jsonArray[i].replace("\"", "");
                jsonArray[i] = jsonArray[i].replace("{pemain:", "");
                jsonArray[i] = jsonArray[i].replace("}", "");
                jsonArray[i] = jsonArray[i].replace("skor:", "");
            }

            for (String i:jsonArray) {
                System.out.println(i);
            }

            for (int i = 0; i < jsonArray.length; i += 2) {
                pemainSkorMap.put(jsonArray[i], Integer.parseInt(jsonArray[i + 1]));
            }
            return pemainSkorMap;
        } finally {
            is.close();
        }
    }

    public static void end() {
        arrayPemain = new JSONArray();
        pemainSkorMap.clear();
    }
}
