package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.pramukainfo.core.CommandMap;

public class NotebookCommand extends Command implements IProcessingCommand {

    public NotebookCommand() {
        this.name = "notebook";
        this.aliases = new String[] { "nb" };
        this.arguments =
        "\n" +
        "  c | Start a notebook\n" +
        "  o | Open your notebook\n" +
        "  toc | Show Table of Contents\n" +
        "  d | Delete your notebook\n" +
        "  f <num> | Open page from notebook\n" +
        "  rm <num> | Remove page from notebook\n" +
        "  ai <keyword> | Instantly add found info to notebook\n";
        this.help = "Put your newfound infos in our notebook system";
    }

    @Override
    protected void execute(CommandEvent event) {
        String user = event.getAuthor().getId();
        String[] args = event.getArgs().split("\\s", 2);
        process(event, insertUserId(args, user));
    }

    private String[] insertUserId(String[] args, String user) {
        if (args.length == 1) args = new String[] { args[0], user };
        else args[1] = user + " " + args[1];
        return args;
    }

    @Override
    public CommandMap getMap() {
        return CommandMap.NOTEBOOK_COMMANDS;
    }
}
