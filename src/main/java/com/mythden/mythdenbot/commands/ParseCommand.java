package com.mythden.mythdenbot.commands;

import com.github.ygimenez.method.Pages;
import com.github.ygimenez.model.Page;
import com.github.ygimenez.type.PageType;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.parsetranslator.translator.FacadeTranslator;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.MessageBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;

import java.util.ArrayList;

public class ParseCommand extends Command {

    public ParseCommand() {
        this.name = "parse";
        this.help = "translate to codes";
        this.arguments = "<from> <to> <string>";
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s+", 3);
        ArrayList<String> arr_string = FacadeTranslator.translateArray(args[0], args[1], args[2]);
        ArrayList<String> raw = FacadeTranslator.raw(args[0], args[1], args[2]);
        String name = args[1] + " code of " + args[2] + " translation";

        ArrayList<Page> pages = new ArrayList<>();
        MessageBuilder mb = new MessageBuilder();
        int size = arr_string.size();

        if(size == 1) {
            mb.append("Cipher Translation" + "\n");
            mb.append("Raw Code: " + raw.get(0) + "\n");
            mb.append("Deciphered Code: " + arr_string.get(0));
            event.reply(mb.build());
        } else {
            for (int i = 0; i < size; i++) {
                mb.clear();
                mb.append("Cipher Translation" + "\n");
                mb.append("Raw Code: " + raw.get(i) + "\n");
                mb.append("Deciphered Code: " + arr_string.get(i));
                pages.add(new Page(PageType.TEXT, mb.build()));
            }
            event.getChannel().sendMessage((Message) pages.get(0).getContent()).queue(success -> {
                Pages.paginate(success, pages);
            });
        }
    }
}
