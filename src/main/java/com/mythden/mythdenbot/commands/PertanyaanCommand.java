package com.mythden.mythdenbot.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwebpp.crypto.TweetNaclFast;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.jsonparser.JsonParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
//import com.mythden.mythdenbot.parser.CommandParser;

public class PertanyaanCommand extends Command {

    public PertanyaanCommand() {
        this.name = "mulai";
        this.aliases = new String[]{"lanjut"};
        this.help = "Play a mini games";
    }

    @Override
    protected void execute(CommandEvent event) {
        ArrayList<String> listPemain = getPemainHelper();
        if (listPemain.contains(event.getAuthor().getName())) {
            String pertanyaan = getPertanyaanHelper();
            System.out.println(pertanyaan);
            System.out.println(listPemain);
            event.reply(pertanyaan);

            if (pertanyaan.equals("Permainan sudah berakhir")) {
                EndCommand endCommand = new EndCommand();
                endCommand.execute(event);
            }
        } else {
            event.reply("Kamu tidak masuk ke permainan ini!");
        }
    }

    public String getPertanyaanHelper() {
        try {
            JSONObject pertanyaanObj = JsonParser.readJsonFromUrl(
//                    "http://127.0.0.1:5050/get-question");
                    "https://pramuka-bot-minigames.herokuapp.com/get-question");
            return pertanyaanObj.getString("pertanyaan");
        } catch (IOException e) {
            System.out.println("");
        }
        return "";
    }

    public ArrayList<String> getPemainHelper() {
        ArrayList<String> pemainList = new ArrayList<>();
        try {
            HashMap<String, Integer> pemainSkorMap = JsonParser.readJsonFromUrlToArray(
//                    "http://127.0.0.1:5050/get-player");
                    "https://pramuka-bot-minigames.herokuapp.com/get-player");

            for (String key:pemainSkorMap.keySet()) {
                pemainList.add(key);
            }
        } catch (Exception e) {
            pemainList.add("empty");
        } finally {
            return pemainList;
        }
    }
}
