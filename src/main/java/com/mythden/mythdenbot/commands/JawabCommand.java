package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.jsonparser.JsonParser;
import com.mythden.mythdenbot.jsonparser.PramukaResponseHandler;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class JawabCommand extends Command {

    public JawabCommand() {
        this.name = "jawab";
        this.help = "Jawab mini games";
    }

    @Override
    protected void execute(CommandEvent event) {
        String user = event.getAuthor().getName();
        boolean found = getPemainHelper().stream().anyMatch(s -> s.contains(user));

        if (found) {
            String answer = event.getArgs();
            String jawaban = jawabHelper(answer, user);
            ArrayList<String> listSkor = getPemainHelper();
            event.reply(jawaban + "\n\nSkor\n");

            for (String skor : listSkor) {
                event.reply(skor);
            }

            if (jawaban.contains("Benar")) {
                PertanyaanCommand pertanyaanCommand = new PertanyaanCommand();
                pertanyaanCommand.execute(event);
            } else if (jawaban.contains("Salah")) {
                event.reply(getPertanyaanKiniHelper());
            }
        }
    }

    public String jawabHelper(String answer, String user) {
        JSONObject json = new JSONObject();
        String response = "";
        json.put("jawaban", answer);

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        try {
//            HttpPost request = new HttpPost("http://127.0.0.1:5050/answer/" + user);
            HttpPost request = new HttpPost("http://pramuka-bot-minigames.herokuapp.com/answer/" + user);
            StringEntity params = new StringEntity(json.toString());
            request.addHeader("content-type", "application/json");
            params.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            request.setEntity(params);
            ResponseHandler<String> responseHttp = new PramukaResponseHandler();
            response = httpClient.execute(request, responseHttp);
            System.out.println(json);
            System.out.println(params);
            System.out.println(response);
            // handle response here...

        } catch (Exception ex) {
            // handle exception here
        } finally {
            try {
                httpClient.close();
            } catch (Exception e) {

            }
        }
        return response;
    }

    public ArrayList<String> getPemainHelper() {
        ArrayList<String> pemainList = new ArrayList<>();
        try {
            HashMap<String, Integer> pemainSkorMap = JsonParser.readJsonFromUrlToArray(
//                    "http://127.0.0.1:5050/get-player");
                    "https://pramuka-bot-minigames.herokuapp.com/get-player");

            for (Map.Entry<String, Integer> entry : pemainSkorMap.entrySet()) {
                pemainList.add(entry.getKey() + ": " + entry.getValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pemainList;
    }

    public String getPertanyaanKiniHelper() {
        try {
            JSONObject pertanyaanObj = JsonParser.readJsonFromUrl(
//                    "http://127.0.0.1:5050/get-exist-question");
                    "https://pramuka-bot-minigames.herokuapp.com/get-exist-question");
            return pertanyaanObj.getString("pertanyaan");
        } catch (IOException e) {
            System.out.println("");
        }
        return "";
    }
}
