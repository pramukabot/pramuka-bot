package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.pramukainfo.core.CommandMap;
import com.mythden.mythdenbot.pramukainfo.core.CommandParser;
import com.mythden.mythdenbot.pramukainfo.core.ReplyProcessor;
import com.mythden.mythdenbot.pramukainfo.core.reply.Reply;

import java.util.List;

public interface IProcessingCommand {

    CommandMap getMap();

    default void process(CommandEvent event, String[] args) {
        List<Reply> replies = CommandParser.getReply(args, getMap());
        ReplyProcessor.produceReply(event, replies);
    }
}
