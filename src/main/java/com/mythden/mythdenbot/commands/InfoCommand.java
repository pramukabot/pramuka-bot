package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.pramukainfo.core.CommandMap;

public class InfoCommand extends Command implements IProcessingCommand {

    public InfoCommand() {
        this.name = "info";
        this.arguments =
        "\n"+
        "  f <id> | Fetch an info\n"+
        "  s <keyword> | Search something\n"+
        "  d <id> | Delete an info\n"+
        "  u <id> <title>[newline]<content> | Update an info\n"+
        "  c <title>[newline]<content> | Create an info\n"+
        "  a | Show all infos\n";
        this.help = "Play around with our information system";
    }

    @Override
    protected void execute(CommandEvent event) {
        String[] args = event.getArgs().split("\\s", 2);
        process(event, args);
    }

    @Override
    public CommandMap getMap() {
        return CommandMap.INFO_COMMANDS;
    }
    
}
