package com.mythden.mythdenbot.commands.music;

import com.mythden.mythdenbot.lavaplayer.GuildMusicManager;
import com.mythden.mythdenbot.lavaplayer.PlayerManager;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.requests.restaction.MessageAction;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class StopCommand extends Command {

    public StopCommand() {
        this.name = "stop";
        this.help = "Stops playing music and clears queue";
    }

    @Override
    protected void execute(CommandEvent event) {
        final TextChannel channel = (TextChannel) event.getChannel();
        final Member self = event.getSelfMember();
        final GuildVoiceState selfVoiceState = self.getVoiceState();

        if(!selfVoiceState.inVoiceChannel()) {
            channel.sendMessage("I need to be in a voice channel!").queue();
            return;
        }

        final Member member = event.getMember();
        final GuildVoiceState memberVoiceState = member.getVoiceState();

        if (!memberVoiceState.inVoiceChannel()) {
            channel.sendMessage("You need to be in a voice channel!").queue();
            return;
        }

        if (!memberVoiceState.getChannel().equals(selfVoiceState.getChannel())) {
            channel.sendMessage("You need to be in the same voice channel as me!").queue();
            return;
        }

        final GuildMusicManager musicManager = PlayerManager.getInstance().getMusicManager(event.getGuild());
        final AudioPlayer audioPlayer = musicManager.audioPlayer;

        final BlockingQueue<AudioTrack> played = musicManager.schedule.played;
        final AudioTrack nowTrack = audioPlayer.getPlayingTrack();
        played.offer(nowTrack);
        final List<AudioTrack> trackList = new ArrayList<>(played);
        final MessageAction messageAction = channel.sendMessage("**Played songs:**\n");

        for (int i = 0; i < played.size(); i++) {
            final AudioTrack track = trackList.get(i);
            final AudioTrackInfo info = track.getInfo();
            messageAction.append('#')
                    .append(String.valueOf(i+1))
                    .append(" `")
                    .append(info.title)
                    .append(" by ")
                    .append(info.author)
                    .append("\n");
        }

        messageAction.queue();

        musicManager.schedule.player.stopTrack();
        musicManager.schedule.queue.clear();
        musicManager.schedule.played.clear();
        channel.sendMessage("The player has been stopped and queue is clear!").queue();
    }
}
