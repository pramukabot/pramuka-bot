package com.mythden.mythdenbot.commands.music;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.lavaplayer.GuildMusicManager;
import com.mythden.mythdenbot.lavaplayer.PlayerManager;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.managers.AudioManager;

public class LeaveCommand extends Command {

    public LeaveCommand() {
        this.name = "leave";
        this.help = "leaves voice channel";
    }

    @Override
    protected void execute(CommandEvent event) {
        final TextChannel channel = (TextChannel) event.getChannel();
        final Member self = event.getSelfMember();
        final GuildVoiceState selfVoiceState = self.getVoiceState();

        if(!selfVoiceState.inVoiceChannel()) {
            channel.sendMessage("I need to be in a voice channel!").queue();
            return;
        }

        final Member member = event.getMember();
        final GuildVoiceState memberVoiceState = member.getVoiceState();

        if (!memberVoiceState.inVoiceChannel()) {
            channel.sendMessage("You need to be in a voice channel!").queue();
            return;
        }

        if (!memberVoiceState.getChannel().equals(selfVoiceState.getChannel())) {
            channel.sendMessage("You need to be in the same voice channel as me!").queue();
            return;
        }

        final Guild guild = event.getGuild();
        final GuildMusicManager musicManager = PlayerManager.getInstance().getMusicManager(event.getGuild());
        musicManager.schedule.repeating = false;
        musicManager.schedule.queue.clear();
        musicManager.schedule.played.clear();
        musicManager.audioPlayer.stopTrack();

        final AudioManager audioManager = event.getGuild().getAudioManager();
        audioManager.closeAudioConnection();
        channel.sendMessage("I have left the voice channel. Bye!").queue();
    }
}
