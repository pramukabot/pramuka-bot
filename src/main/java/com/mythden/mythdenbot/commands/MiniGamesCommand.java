package com.mythden.mythdenbot.commands;

import com.fasterxml.jackson.annotation.JsonValue;
import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.jagrosh.jdautilities.commons.waiter.EventWaiter;
import com.mythden.mythdenbot.jsonparser.JsonParser;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MiniGamesCommand extends Command {

    public MiniGamesCommand() {
        this.name = "main";
        this.help = "Mulai permainan";
    }

    @Override
    protected void execute(CommandEvent event) {
        String pemain = event.getAuthor().getName();
        jsonHelper(pemain);
        ArrayList<String> response = getPemainHelper();
        int num = 0;
        event.reply("Jumlah pemain: " + String.valueOf(response.size()));
        for (String namaPemain:response) {
            num++;
            event.reply(num + ". " + namaPemain);
        }
        response.clear();
    }

    public void jsonHelper(String pemain) {
        JSONObject json = new JSONObject();
        json.put("pemain", pemain);
        json.put("skor", 0);

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        try {
//            HttpPost request = new HttpPost("http://127.0.0.1:5050/play");
            HttpPost request = new HttpPost("https://pramuka-bot-minigames.herokuapp.com/play");
            StringEntity params = new StringEntity(json.toString());
            request.addHeader("content-type", "application/json");
            params.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            request.setEntity(params);
            httpClient.execute(request);
            System.out.println(params);
            // handle response here...
        } catch (Exception ex) {
            // handle exception here
        } finally {
            try {
                httpClient.close();
            } catch (Exception e) {

            }
        }
    }

    public ArrayList<String> getPemainHelper() {
        ArrayList<String> pemainList = new ArrayList<>();
        try {
            HashMap<String, Integer> pemainSkorMap = JsonParser.readJsonFromUrlToArray(
//                    "http://127.0.0.1:5050/get-player");
                    "https://pramuka-bot-minigames.herokuapp.com/get-player");

            for (Map.Entry<String, Integer> entry : pemainSkorMap.entrySet()) {
                pemainList.add(entry.getKey());
            }
            pemainSkorMap.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pemainList;
    }
}
