package com.mythden.mythdenbot.commands;

import com.jagrosh.jdautilities.command.Command;
import com.jagrosh.jdautilities.command.CommandEvent;
import com.mythden.mythdenbot.jsonparser.JsonParser;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;
//import com.mythden.mythdenbot.parser.CommandParser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EndCommand extends Command {

    public EndCommand() {
        this.name = "end";
        this.help = "End mini games";
    }

    @Override
    protected void execute(CommandEvent event) {
        ArrayList<String> result = getPemainHelper();
        endHelper();

        event.reply("Permainan berakhir!");
        for (String skor:result) {
            event.reply(skor);
        }
        JsonParser.end();
    }

    public String endHelper() {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        try {
//            HttpGet request = new HttpGet("http://127.0.0.1:5050/end-game");
            HttpGet request = new HttpGet("https://pramuka-bot-minigames.herokuapp.com/end-game");
            httpClient.execute(request);
            httpClient.close();

        } catch (IOException e) {
            System.out.println("");
        }
        return "";
    }

    public ArrayList<String> getPemainHelper() {
        ArrayList<String> pemainList = new ArrayList<>();
        try {
            HashMap<String, Integer> pemainSkorMap = JsonParser.readJsonFromUrlToArray(
//                    "http://127.0.0.1:5050/get-player");
                    "https://pramuka-bot-minigames.herokuapp.com/get-player");

            for (Map.Entry<String, Integer> entry : pemainSkorMap.entrySet()) {
                pemainList.add(entry.getKey() + ": " + entry.getValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return pemainList;
    }
}
